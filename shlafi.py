# Copyright by Grzesiek11 and TadeLn 2019
# Licensed under GNU GPL v3.0.
# Random Language File Generator for Minecraft
# TadeLn hopes that PhoenixSC will see that :P

import json, random, re, sys, os

# Open original language file.
with open(sys.argv[1]) as file:
    lang = json.load(file)

# Swap random values in language file.
for item in lang.items():
    # Get second (random) item from language file.
    item2 = random.choices(list(lang.items()))[0]
    # Swap items.
    temp = item[1]
    lang[item[0]] = item2[1]
    lang[item2[0]] = temp

# Use regex to remove anything like '%s' to avoid errors in Minecraft.
for value in list(lang.values()):
    re.sub("\%+\w?\$*\w", '', value)

# Dump results to a new file.
with open(f"{os.path.splitext(sys.argv[1])[0]}_shuffled.json", 'w') as file:
    json.dump(lang, file)
