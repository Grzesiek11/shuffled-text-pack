# Shuffled Language File Generator for Minecraft

## What is it?

This lil' script swaps random lines in Minecraft language files. It leads to kind of funny names in-game.

[reddit post](https://www.reddit.com/r/PhoenixSC/comments/cpgk0h/shuffled_language_file/?utm_source=share&utm_medium=web2x)

![](screenshot.png)

## Example resource pack

TadeLn made an resource pack with example generated language file:

[Download](https://drive.google.com/open?id=158I8qDSqZz4p_j1-H0Qk76G3H0IuAbQI)

## How to use it?

You need two things:

1. Minecraft's language file.
2. Python 3 (I recommend 3.6 or higher).

Then run script from command line (In Windows, replace `python3` with `python`):

```
python3 shlafi.py /path/to/language/file
```

Then, you can make an resource pack with that file.

Have fun! Leave a star on this repo and upvote TadeLn's reddit post if you enjoyed.

## Authors

Grzesiek11 and TadeLn.
